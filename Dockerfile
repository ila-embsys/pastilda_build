FROM ubuntu:bionic

RUN apt-get update \
	&& apt-get install -y --no-install-recommends \
	wget \
	ca-certificates \
	gnupg2

# Install dependencies
RUN apt-get update \
# Toolchain
	&& apt-get install -y --no-install-recommends \
# Builders
	ninja-build \
	make \
# Git
    git \
    git-lfs \
# pip3 dependencies
    python3-pip \
    python3-setuptools \
# Needs for to compile libopencm3
	python2.7 \
###
	&& ln -s /usr/bin/python2.7 /usr/bin/python
	
# Path to stored in repo tools
ARG TOOLS_PATH="tools"

# Add CMake
ARG CMAKE_TOOL="cmake-3.16.3-Linux-x86_64"
ARG CMAKE_ARCHIVE="$CMAKE_TOOL.tar.gz"

COPY ${TOOLS_PATH}/$CMAKE_ARCHIVE /
RUN echo "Unpacking $CMAKE_ARCHIVE... " \
	&& tar -zxf $CMAKE_ARCHIVE \
	&& echo "Done!" \
	&& rm /$CMAKE_ARCHIVE
ENV PATH="/${CMAKE_TOOL}/bin:${PATH}"

RUN wget --progress=dot:giga https://armkeil.blob.core.windows.net/developer/Files/downloads/gnu-rm/9-2019q4/RC2.1/gcc-arm-none-eabi-9-2019-q4-major-x86_64-linux.tar.bz2 \
	&& echo "Unpacking... " \
	&& tar -xjf gcc-arm-none-eabi-9-2019-q4-major-x86_64-linux.tar.bz2 \
	&& echo "Done" \
	&& rm gcc-arm-none-eabi-9-2019-q4-major-x86_64-linux.tar.bz2

ENV PATH="/gcc-arm-none-eabi-9-2019-q4-major/bin:${PATH}"

# Config Git to prevent error on message "Please tell me who you are."
RUN git config --global user.email "build@build" \
    && git config --global user.name "CI Builder"

